import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'http://dashboard-keluargasehat.kemkes.go.id/rest/'

export const store = new Vuex.Store({
    state:{
        token : localStorage.getItem('access_token'),
    },
    getters:{
        loggedIn(state){
            return state.token
        },
    },
    mutations:{
        retrievetoken(state, token){
            state.token = token
        },
    },
    actions:{
        retrieveToken(context, credentials){
            axios.post('api/auth/login',{
                username : credentials.username,
                password : credentials.password,
            })
            .then(response => {
                const token = response.data.data.token
                const tes = response.config.data
                localStorage.setItem('access_token', token)
                context.commit('retrievetoken', token)
                console.log(localStorage.getItem('access_token'))

            })
            .catch(error => {
                console.log("error")
            })
        },
        retrieveUser(){
            var config = {
                headers: {  
                            Authorization: 'Bearer '+localStorage.getItem('access_token'),
                            'Content-Type': 'application/json',    
                         }
            };
            axios.get('api/auth/user',config)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
        },
        retrieveData(){
            var config = {
                headers: {  'access-control-allow-origin': '*',
                            Authorization : 'Bearer '+localStorage.getItem('access_token'),
                            'content-type': 'application-json',    
                         }
            };
            axios.get('api/report',config)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
        },
        destroyToken(){
            var config = {
                headers: {  
                            Authorization: 'Bearer '+localStorage.getItem('access_token'),
                            'Content-Type': 'application/json',    
                         }
            };
            axios.delete('api/auth/invalidate',config)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
        },
    }
})